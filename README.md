# **About  Android Table Create (ATC)** # 
Android Table Create, is table creation library for android using annotation. Now you don't need to write lengthy queries to create tables, Just annotate your table class with JPA like annotations, ATR will create queries it self.

### **Version 1.0** ###

### Supported Annotations ###


```
#!java

 * @Entity
 * @Column
 * @Id
 * @GeneratedValue
 * @ForeginKey
 * @Transient
```


**Create Table Example**

**Table 1**


```
#!java

@Entity(name="BIO_DATA")
public class Table{
 
  @Id
  private long pk;
  
  @Column(name="USER_NAME")
  private String name;
  
  @ForeignKey
  @Column(name="fk_user_address")
  private long fk_address;
  
}
```


**Table 2**



```
#!java

@Entity(name = "Supported_Types")
public class TestTable_DataType {

	@Id
	private long id;

        @Column(name="CUSTOM_NAME")
	private String string;

	private int primInt;

	private double primDouble;

	private boolean primBoolean;

	private char primChar;

	private float primFloat;

	private Character objCharacter;

	private Integer objInteger;

	private Float objFLoat;

	private Double objDouble;

	private Long objLong;

	private Number objNumber;

	private BigDecimal bigDecimal;

	private Calendar objCalendar;

	private Date objDate;

}

```


***Crate Queries Code**


 		
```
#!java

QueryGenerator queryGenerator = new QueryGenerator();
 		queryGenerator.addClass(TestTable_DataType.class);
 		queryGenerator.addClass(Table.class);
 		try {
 			List<String> queries = queryGenerator.createQueries();
 			for (String query : queries) {
 				System.out.println(query); 			}
		} catch (QueryFormationException e) {

			System.out.println(e.getMessage());
 		}
```




**Output**



```
#!sql

CREATE TABLE BIO_DATA(pk INTEGER PRIMARY KEY NOT NULL  , USER_NAME TEXT , fk_user_address INTEGER NOT NULL)


CREATE TABLE Supported_Types(id INTEGER PRIMARY KEY NOT NULL  , CUSTOM_NAME TEXT , primInt INTEGER , primDouble REAL , primBoolean INTEGER , primChar TEXT , primFloat REAL , objCharacter TEXT , objInteger INTEGER , objFLoat REAL , objDouble REAL , objLong INTEGER , objNumber TEXT , bigDecimal TEXT , objCalendar TEXT , objDate TEXT)




```
