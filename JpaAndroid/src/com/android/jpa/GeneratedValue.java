/*Copyright (C) 2014  Yahya Arshad, Email: Yahya.Arshad@Gmail.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.android.jpa;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * (Optional) The primary key generation strategy default is
 * <code> GenerationType.IDENTITY</code> which means AUTOINCREMENT
 * <p><b>Example:</b></p>
 *  
* <pre>
*  
* @Entity(name="my_table")
* public class Table{
* 
* @Id
* @GeneratedValue(strategy = GenerationType.IDENTITY)
* private long id;
* 
* 
* }
* </pre>
 * 
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
public @interface GeneratedValue {

	GenerationType strategy() default GenerationType.IDENTITY;

}
