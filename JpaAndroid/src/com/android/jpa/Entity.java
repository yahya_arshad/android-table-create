/*Copyright (C) 2014  Yahya Arshad, Email: Yahya.Arshad@Gmail.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.android.jpa;


import java.lang.annotation.Target;
import java.lang.annotation.Retention;
import java.lang.annotation.Documented;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;


/**
 * (Require) Specifies that the class is a table. This annotation is applied to
 * the table
 * class.
 * <p>
 * <b>Example:</b><code> @Entity(name="table_name")</code> if name attribute is
 * not specified class name will be used as table name
 */
@Documented
@Target(TYPE)
@Retention(RUNTIME)
public @interface Entity {

	String name() default "";
}
