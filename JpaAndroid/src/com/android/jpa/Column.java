/*Copyright (C) 2014  Yahya Arshad, Email: Yahya.Arshad@Gmail.com

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.android.jpa;


import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;


/**
 * 
 * 
 * (Optional) annotation, if not specified or specified without column
 * name, field name will be used as column name other wise if specified
 * as @Column(name="col_name") name will be used as column name
 * 
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD })
public @interface Column {

	/**
	 * (Optional) The name of the column. Defaults to the property or field
	 * name.
	 */
	String name() default "";

	/**
	 * (Optional) Whether the database column is nullable true is default.
	 */
	boolean nullable() default true;

	String columnDefinition() default "";

}
