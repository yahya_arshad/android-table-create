package com.android.jpa.test;


import java.util.List;


import com.android.jpa.QueryFormationException;
import com.android.jpa.QueryGenerator;


public class JPATest {

	public static void main(String[] args) {

		QueryGenerator queryGenerator = new QueryGenerator();
		queryGenerator.addClass(TestTable_DataType.class);
		queryGenerator.addClass(Table.class);
	 
		 
		try {
			List<String> queries = queryGenerator.createQueries();
			for (String query : queries) {
				System.out.println(query);
			}
		} catch (QueryFormationException e) {

			System.out.println(e.getMessage());
		}

	}

}
