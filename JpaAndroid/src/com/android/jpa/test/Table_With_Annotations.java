package com.android.jpa.test;

import com.android.jpa.Column;
import com.android.jpa.Entity;
import com.android.jpa.Id;

@Entity(name = "ANNOTATED_TABLE")
public class Table_With_Annotations {

	@Id
	@Column(name = "_id")
	private int id;

}
