package com.android.jpa.test;


import com.android.jpa.Column;
import com.android.jpa.Entity;
import com.android.jpa.ForeignKey;
import com.android.jpa.Id;


@Entity(name = "BIO_DATA")
public class Table {

	@Id
	private long pk;

	@Column(name = "USER_NAME")
	private String name;

	@ForeignKey
	@Column(name = "fk_user_address")
	private long fk_address;
}
