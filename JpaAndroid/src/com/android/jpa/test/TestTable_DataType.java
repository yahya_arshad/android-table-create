package com.android.jpa.test;


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;




import com.android.jpa.Column;
import com.android.jpa.Entity;
import com.android.jpa.Id;


@Entity(name = "Supported_Types")
public class TestTable_DataType {

	@Id
	private long id;

	@Column(name="CUSTOM_NAME")
	private String string;

	private int primInt;

	private double primDouble;

	private boolean primBoolean;

	private char primChar;

	private float primFloat;

	private Character objCharacter;

	private Integer objInteger;

	private Float objFLoat;

	private Double objDouble;

	private Long objLong;

	private Number objNumber;

	private BigDecimal bigDecimal;

	private Calendar objCalendar;

	private Date objDate;

}
