package com.android.jpa.test;

import com.android.jpa.Column;
import com.android.jpa.Entity;
import com.android.jpa.ForeignKey;

@Entity
public class ChildRecord {
	
	@ForeignKey
	@Column(name="fk_master")
	private MasterRecord parent;

}
